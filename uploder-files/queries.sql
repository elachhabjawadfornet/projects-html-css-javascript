CREATE DATABASE IF NOT EXISTS uploader_files;

USE uploader_files;


CREATE TABLE IF NOT EXISTS uploading (
id int(11) NOT NULL AUTO_INCREMENT,
file_name varchar(100) COLLATE utf8_unicode_ci NOT NULL,
PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;